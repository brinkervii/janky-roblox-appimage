export DEBIAN_FRONTEND

apt-get update
apt-get install -y wget git python3 python3-pip python3-dev libfuse2 p7zip-full

APPIMAGE_TOOL=./appimagetool.AppImage
APPDIR=roblox-app.AppDir

wget https://github.com/AppImage/AppImageKit/releases/download/13/appimagetool-x86_64.AppImage -O "$APPIMAGE_TOOL"
chmod +x "$APPIMAGE_TOOL"

mkdir "$APPDIR/python"
pip install git+https://gitlab.com/brinkervii/grapejuice.git --target="$APPDIR/python"

cp "$APPDIR/python/grapejuice_common/assets/icons/hicolor/256x256/apps/grapejuice-roblox-player.png" "$APPDIR/roblox.png"

cp "$APPDIR/AppRun.in" "$APPDIR/AppRun"
chmod +x "$APPDIR/AppRun"
ARCH=x86_64 $APPIMAGE_TOOL "$APPDIR"

mkdir output
mv Roblox_App*.AppImage output